
package rockPaperScissors;
//import java.util.Arrays;
//import java.util.List;
import java.util.Scanner;
import java.util.Random;
public class RockPaperScissors {
    
    public static void main(String[] args) {
        /*  
         * The code here does two things:
         * It first creates a new RockPaperScissors -object with the
         * code `new RockPaperScissors()`. Then it calls the `run()`
         * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    int playround = 1;
    int humanScore = 0;
    int computerScore = 0;

    public void run() {
        String[] rps = {"rock", "paper", "scissors"};
        String computerChose = rps[new Random().nextInt(rps.length)];
        Scanner scanner = new Scanner(System.in);
        String playerChose;
        while(true) { 
            System.out.printf("Let's play round %d \n", playround);
            // Human input scanner + check
            while(true) {
                System.out.println("Your choice (Rock/Paper/Scissors)?");
                playerChose = scanner.nextLine();
                playerChose.toLowerCase();
                if (playerChose.equals("rock") || playerChose.equals("paper") || playerChose.equals("scissors")) {
                    break;
                }
                System.out.println("I do not understand " + playerChose + ". Could you try again?");
            }

            // Who wins?
            // System.out.println("Computer played " + computerChose);
            
            if (playerChose.equals(computerChose)) {
                System.out.printf("Human chose %s, computer chose %s. It's a tie!\n", playerChose, computerChose);
            } else if (playerChose.equals("rock")){
                if (computerChose.equals("scissors")){
                    System.out.printf("Human chose %s, computer chose %s. Human wins!\n", playerChose, computerChose);
                    humanScore ++;
                } else if (computerChose.equals("paper")){
                    System.out.printf("Human chose %s, computer chose %s. Computer wins!\n", playerChose, computerChose);
                    computerScore ++;
                }
            } else if (playerChose.equals("paper")){
                if (computerChose.equals("rock")){
                    System.out.printf("Human chose %s, computer chose %s. Human wins!\n", playerChose, computerChose);
                    humanScore ++;
                } else if (computerChose.equals("scissors")){
                    System.out.printf("Human chose %s, computer chose %s. Computer wins!\n", playerChose, computerChose);
                    computerScore++;
                }
            } else if (playerChose.equals("scissors")){
                if (computerChose.equals("paper")){
                    System.out.printf("Human chose %s, computer chose %s. Human wins!\n", playerChose, computerChose);
                    humanScore ++;
                } else if (computerChose.equals("rock")){
                    System.out.printf("Human chose %s, computer chose %s. Computer wins!\n", playerChose, computerChose);
                    computerScore ++;
                }
            }

        
            System.out.printf("Score: human %d, computer %d \n", humanScore, computerScore);
            System.out.println("Do you wish to continue playing? (y/n)?");
            String playAgain = scanner.nextLine();
            if (!playAgain.equals("y")){
                System.out.println("Bye bye :)");
                break;
            }
            
            playround++;
        }
        scanner.close();
    }
}
/*
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
*/